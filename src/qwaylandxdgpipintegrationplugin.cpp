/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "qwaylandxdgpipintegration_p.h"
#include <QtWaylandClient/private/qwaylandshellintegrationplugin_p.h>

using namespace XdgPipQt;

class QWaylandXdgPipIntegrationPlugin : public QtWaylandClient::QWaylandShellIntegrationPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QWaylandShellIntegrationFactoryInterface_iid FILE "xdg-pip.json")

public:
    QWaylandXdgPipIntegrationPlugin()
    {
    }

    QtWaylandClient::QWaylandShellIntegration *create(const QString &key, const QStringList &paramList) override
    {
        Q_UNUSED(key);
        Q_UNUSED(paramList);
        return new QWaylandXdgPipIntegration();
    }
};

// Q_IMPORT_PLUGIN(QWaylandXdgPipIntegrationPlugin);

#include "qwaylandxdgpipintegrationplugin.moc"
