/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "qwaylandxdgpip_p.h"
#include <private/qwaylandwindow_p.h>

namespace XdgPipQt
{

class QWaylandXdgPip : public QtWayland::xdg_pip_v1
{
public:
    QWaylandXdgPip(struct ::xdg_pip_v1 *object);
    ~QWaylandXdgPip() override;

    void move(int serial);
    void resize(int serial, int edge);

    void xdg_pip_v1_configure_bounds(int32_t width, int32_t height) override;
    void xdg_pip_v1_configure(int32_t width, int32_t height) override;
    void xdg_pip_v1_dismissed() override;

    QSize m_pendingSize;
};


QWaylandXdgPip::QWaylandXdgPip(struct ::xdg_pip_v1 *object)
    : QtWayland::xdg_pip_v1(object)
{
}

QWaylandXdgPip::~QWaylandXdgPip()
{
    xdg_pip_v1_destroy(object());
}

void QWaylandXdgPip::xdg_pip_v1_configure_bounds(int32_t width, int32_t height)
{
}

void QWaylandXdgPip::xdg_pip_v1_configure(int32_t width, int32_t height)
{
    m_pendingSize = {width, height};
}

void QWaylandXdgPip::xdg_pip_v1_dismissed()
{
}

QWaylandXdgPipSurface::QWaylandXdgPipSurface(struct ::xdg_pip_v1 *object, QtWaylandClient::QWaylandWindow *window)
    : QWaylandShellSurface(window)
    , d(new QWaylandXdgPip(object))
{}

QWaylandXdgPipSurface::~QWaylandXdgPipSurface() {}

void QWaylandXdgPipSurface::applyConfigure()
{
    window()->resizeFromApplyConfigure(d->m_pendingSize);
}

}
