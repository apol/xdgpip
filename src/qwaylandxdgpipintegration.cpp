/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "qwaylandxdgpip_p.h"
#include "qwaylandxdgpipintegration_p.h"

#include <QWaylandClientExtensionTemplate>
#include <QtWaylandClient/private/qwaylanddisplay_p.h>
#include <QtWaylandClient/private/qwaylandwindow_p.h>
#include <qwayland-xdg-pip-v1.h>

namespace XdgPipQt
{

class XdgWmPipV1 : public QWaylandClientExtensionTemplate<XdgWmPipV1>, public QtWayland::xdg_wm_pip_v1
{
public:
    XdgWmPipV1()
        : QWaylandClientExtensionTemplate(1)
    {
    }

    QtWaylandClient::QWaylandShellSurface *createXdgPip(QtWaylandClient::QWaylandWindow *window) {
        return new XdgPipQt::QWaylandXdgPipSurface(get_xdg_pip(window->wlSurface()), window);
    }
};

QWaylandXdgPipIntegration::QWaylandXdgPipIntegration()
    : m_xdgWmPip(new XdgWmPipV1)
{
}

QWaylandXdgPipIntegration::~QWaylandXdgPipIntegration()
{
}

QtWaylandClient::QWaylandShellSurface *QWaylandXdgPipIntegration::createShellSurface(QtWaylandClient::QWaylandWindow *window)
{
    return m_xdgWmPip->createXdgPip(window);
}

}

// #include "qwaylandxdgpipintegration.moc"
