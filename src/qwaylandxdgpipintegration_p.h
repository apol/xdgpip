/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <wayland-client.h>

#include "xdgpipqt_export.h"
#include <QtWaylandClient/private/qwaylandshellintegration_p.h>

namespace XdgPipQt
{
class QWaylandXdgPip;
class XdgWmPipV1;

class XDGPIPQT_EXPORT QWaylandXdgPipIntegration : public QtWaylandClient::QWaylandShellIntegration
{
public:
    QWaylandXdgPipIntegration();
    ~QWaylandXdgPipIntegration() override;

    QtWaylandClient::QWaylandShellSurface *createShellSurface(QtWaylandClient::QWaylandWindow *window) override;

private:
    QScopedPointer<XdgWmPipV1> m_xdgWmPip;
};

}
