/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <wayland-client.h>
#include <memory>

#include "xdgpipqt_export.h"
#include <QtWaylandClient/private/qwaylandshellsurface_p.h>
#include <qwayland-xdg-pip-v1.h>

namespace XdgPipQt
{
class QWaylandXdgPip;

class XDGPIPQT_EXPORT QWaylandXdgPipSurface : public QtWaylandClient::QWaylandShellSurface
{
public:
    QWaylandXdgPipSurface(struct ::xdg_pip_v1 *object, QtWaylandClient::QWaylandWindow *window);
    ~QWaylandXdgPipSurface() override;

    void applyConfigure() override;

private:
    std::unique_ptr<QWaylandXdgPip> d;
};

}
