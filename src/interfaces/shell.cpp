/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "shell.h"
#include <QByteArray>
#include <qglobal.h>
#include <xdgpipqt_logging.h>

using namespace XdgPipQt;

void Shell::useXdgPip()
{
    const bool ret = qputenv("QT_WAYLAND_SHELL_INTEGRATION", "xdg-pip");
    if (!ret) {
        qCDebug(XDGPIPQT) << "Unable to set QT_WAYLAND_SHELL_INTEGRATION=xdg-pip";
    }
}
