/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef XDGPIPQTWINDOW_H
#define XDGPIPQTWINDOW_H

#include <QObject>
#include <QScreen>
#include <QWindow>

#include "xdgpipqt_export.h"

namespace XdgPipQt
{
class WindowPrivate;

class XDGPIPQT_EXPORT Window : public QObject
{
    Q_OBJECT
public:
    ~Window() override;

    enum class Edge {
        None = 0,
        Top = 1,
        Bottom = 2,
        Left = 4,
        TopLeft = 5,
        BottomLeft = 6,
        Right = 8,
        TopRight = 9,
        BottomRight = 10,
    };
    Q_ENUM(Edge);

    /**
     * Gets the XdgPip Window for a given Qt Window
     * Ownership is not transferred
     */
    static Window *get(QWindow *window);

private:
    Window(QWindow *window);
    QScopedPointer<WindowPrivate> d;
};

}

#endif
