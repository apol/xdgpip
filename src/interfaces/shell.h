/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef XDGPIPQTSHELL_H
#define XDGPIPQTSHELL_H

#include "window.h"
#include "xdgpipqt_export.h"
#include <QString>

namespace XdgPipQt
{
/**
 * Sets the right environment so the shells created from now on use wlr-xdgpip.
 */
class XDGPIPQT_EXPORT Shell
{
public:
    static void useXdgPip();
};

}

#endif
