/*
 *   SPDX-FileCopyrightText: 2023 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <QCommandLineParser>

#include <QGuiApplication>
#include <QPainter>
#include <QRasterWindow>
#include <QTimer>
#include <QWindow>

#include <QMetaEnum>

#include <interfaces/shell.h>
#include <interfaces/window.h>

using namespace XdgPipQt;

class BasicWindow : public QRasterWindow
{
    void paintEvent(QPaintEvent *)
    {
        QPainter p(this);
        p.fillRect(QRect(0, 0, width(), height()), Qt::red);
    }
};

int main(int argc, char **argv)
{
    Shell::useXdgPip();

    QGuiApplication app(argc, argv);

    QCommandLineParser parser;
    QCommandLineOption widthOption(QStringLiteral("width"), QStringLiteral("Width of the window"), QStringLiteral("pixels"), QStringLiteral("0"));
    QCommandLineOption heightOption(QStringLiteral("height"), QStringLiteral("Height of the window"), QStringLiteral("pixels"), QStringLiteral("0"));

    parser.addOptions({widthOption, heightOption});
    parser.addHelpOption();
    parser.process(app);

    BasicWindow window;

    XdgPipQt::Window *xdgpipWindow = XdgPipQt::Window::get(&window);
    Q_ASSERT(xdgpipWindow);
    if (parser.isSet(widthOption)) {
        window.setWidth(parser.value(widthOption).toInt());
    }
    if (parser.isSet(heightOption)) {
        window.setHeight(parser.value(heightOption).toInt());
    }

    window.show();

    // just so you don't block yourself out whilst testing
    QTimer::singleShot(5000, &app, &QGuiApplication::quit);
    return app.exec();
}
